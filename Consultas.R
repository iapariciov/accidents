if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
p_load(dplyr,tidyverse,dplyr,stringr,rgdal,sf,sqldf)

## Carrega de dades preparades

dfaccidents <- read.csv("./salida/accidents_sf.csv",
                        fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                        stringsAsFactors = FALSE)
dfcauses <- read.csv("./salida/accidents_causes.csv",
                     fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                     stringsAsFactors = FALSE)
dfpersones <- read.csv("./salida/accidents_persones.csv",
                       fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                       stringsAsFactors = FALSE)
dftipus <- read.csv("./salida/accidents_tipus.csv",
                    fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                    stringsAsFactors = FALSE)
dfvehicles <- read.csv("./salida/accidents_vehicles.csv",
                       fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                       stringsAsFactors = FALSE)

# Coherència de les dades

# Nombre d'accidents i vehicles implicats segons accidents
sqldf("select sum(N_VEHICLES_IMPLICATS) vehicles, count(*) accidents from dfaccidents")

# nrow(subset(data, x>"0")
nrow(subset(dfaccidents,N_VEHICLES_IMPLICATS> 0))

  # table(data$type, data$x)
table(dfaccidents$NUM_ANY,dfaccidents$N_VEHICLES_IMPLICATS)
table(dfaccidents$CODI_DISTRICTE,dfaccidents$N_MORTS)

# Nombre d'accidents i vehicles implicats segons vehicles
sqldf("select count(*) vehicles, count(distinct NUM_EXPEDIENT) accidents from dfvehicles")

pp <- dfaccidents %>% left_join(dfcauses)
pp <- pp %>% left_join(dfpersones, by="NUM_EXPEDIENT")
pp <- pp %>% left_join(dftipus, by="NUM_EXPEDIENT")
pp <- pp %>% left_join(dfvehicles, by="NUM_EXPEDIENT")

