Proyecto para limpieza de datos de los accidentes de Barcelona según informa Guardia Urbana.

Hay que bajarse los datos de Opendata Barcelona:

http://opendata-ajuntament.barcelona.cat/data/ca/dataset/accidents-gu-bcn

Hay un fichero por cada año desde el 2010 hasta el 2018, a fecha de hoy 1/6/2019.

El procedimiento está esperando que los datos esten en un directorio 'entrada' y los resultados se dejen en un directorio  'salida'

Maig 2019
Confirmat que les dades estan en ED50 i representen la coordenada de la parcel·la a la qual s'ha associat l'accident. 
Aquesta localització té sentit contra la cartografia parcel·lària ED50 perquè cau dins de la parcel·la. 
Per contra, no té cap sentit, o el perd si no es corregeix, si ubiquem aquests punts sobre la cartografia ETRS89 i no fem la rectificació oportuna.

Juny 2019

Se ha reducido el número de ficheros. Se ha limpiado el proyecto.
Ahora tenemos el tratamiento en PreparaAccidents.Rmd que llama a accidents_*.R para tratar los datos
de los otros datasets que tienen relación con el principal.

Se ha confeccionado un csv para cad uno de estos conceptos

accidents_sf: los accidentes y su dimensión territorial (dirección postal y coordenadas) y temporal, con datos de número de afectados de algún tipo.
accidents_causes: causes que han determinado el accidente
accidents_persones: características de las personas implicadas
accidents_tipus: tipología de los accidentes
accidents_vehicles: características de los vehículos implicados

PreparaAccidents.RMD conté el codi R que carrega les dades des d'Opendata, les neteja i les desa a la carpeta de sortida perquè es pugui iniciar els procediments d'anàlisi.

També crida a altres scripts R que s'encarreguen de recuperar altres fitxers de dades relacionats amb els accidents, els neteja i les desa a la sortida per tal que puguin 
ser relacionats amb el principal accidents_sf.

Gener 2021

Refaig les consultes sobre el dataset accidents (versió sense el camp geometria del dataset accidents_sf): Ara les faig amb tidyverse i no amb sqldf.

ACCIDENTS
Dimensió territorial: districte, barri, carrer-número, coordenada
Dimensió temporal: any, mes, dia, hora, torn
Variables discretes: torn, causa_vianant
Variables continues: morts, lesionats grues, lesionats lleus, vehicles implicats

Les quatre taules que hi ha a continuació es relacionen amb accidents a través del NUM_EXEDIENT

CAUSES
causa mediata

PERSONES
causa vianant
tipus vehicle
sexe
tipus persona
edat
victimitzacio
situació

TIPUS ACCIDENT
tipus accident

VEHICLES
causa vianant
tipus vehicles
model
marca
color
carnet
antiguitat carnet



