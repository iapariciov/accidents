library(readr)
library(tidyverse)
library(ggplot2)
library(stringr)
##############################################
# accidents
##############################################

# accidents <- read.csv("./salida/accidents_sf.csv",
#                         fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
#                         stringsAsFactors = FALSE)

accidents <- read.csv("./salida/accidents.csv",
                      fileEncoding="UTF-8",header=TRUE,sep=",",dec='.',
                      stringsAsFactors = FALSE)

acc <- accidents

acc$DATA <- as.Date(acc$DATA, "%Y-%m-%d")

accxdia <- acc %>% 
  group_by(DATA) %>% 
  summarise(
    t_acc = n(),
    t_vic = sum(N_VICTIMES),
    t_mor = sum(N_MORTS),
    t_lle = sum(N_LESIONATS_LLEUS),
    t_gre = sum(N_LESIONATS_GREUS),
    t_veh = sum(N_VEHICLES_IMPLICATS)
  )

accxdia_2010 <- accxdia %>% filter(format(DATA, format = "%Y%m") == "201001") 

ggplot(accxdia_2010, aes(x = DATA)) +
  geom_point(aes(y = t_acc))

accxdiasem <- acc %>% 
  group_by(DESC_DIA_SETMANA) %>% 
  summarise(
    t_acc = n(),
    t_vic = sum(N_VICTIMES),
    t_mor = sum(N_MORTS),
    t_lle = sum(N_LESIONATS_LLEUS),
    t_gre = sum(N_LESIONATS_GREUS),
    t_veh = sum(N_VEHICLES_IMPLICATS)
  )

ggplot(accxdiasem, aes(x=DESC_DIA_SETMANA)) +
  geom_point(aes(y = t_acc))

accxhora <- acc %>% 
  group_by(HORA_DIA) %>% 
  summarise(
    t_acc = n(),
    t_vic = sum(N_VICTIMES),
    t_mor = sum(N_MORTS),
    t_lle = sum(N_LESIONATS_LLEUS),
    t_gre = sum(N_LESIONATS_GREUS),
    t_veh = sum(N_VEHICLES_IMPLICATS)
  )

ggplot(accxhora, aes(x=HORA_DIA)) +
  geom_point(aes(y = t_acc))

