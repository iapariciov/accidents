## https://ryanpeek.github.io/2017-08-03-converting-XY-data-with-sf-package/
# Exercicis de l'article

if (!require("devtools")) install.packages("devtools"); require(devtools)
if (!require("pacman")) install_version("pacman", version = "0.4.1"); require(pacman)
if (!require("htmltab")) install_version("htmltab"); require(htmltab)

suppressMessages({
  library(tidyverse)
  library(sf)
  library(htmltab)
})

url <- "http://www.hotspringsdirectory.com/usa/ca/gps-usa-ca.html"
df <- htmltab(url, which=1, header = 1,
              colNames = c("STATE","LAT","LONG","SpringName","Temp_F", "Temp_C", "AREA", "USGS_quad") ) # get the data

# convert columns to numeric:
sapply(df, class)

cols.num<- c("LAT","LONG", "Temp_F", "Temp_C")
df[cols.num]<-sapply(df[cols.num], as.numeric) # some NA's where there are no temperatures available

df$LONG<-df$LONG*(-1) # because the table doesn't include this important bit
head(df)

# make the UTM cols spatial (X/Easting/lon, Y/Northing/lat)
df.SP <- st_as_sf(df, coords = c("LONG", "LAT"), crs = 4326)

st_crs(32610) ## more detailed def (UTMs)

st_crs(df.SP)

# transform to UTM
df.SP<-st_transform(x = df.SP, crs = 32610)

# get coordinates and add back to dataframe
df.SP$utm_E<-st_coordinates(df.SP)[,1] # get coordinates
df.SP$utm_N<-st_coordinates(df.SP)[,2] # get coordinates

# now switch back to lat-long
df.SP<-st_transform(x = df.SP, crs = 4326)

# add coordinates to dataframe
df.SP$lon<-st_coordinates(df.SP)[,1] # get coordinates
df.SP$lat<-st_coordinates(df.SP)[,2] # get coordinates

# coerce back to data.frame:
df.SP<-st_set_geometry(df.SP, NULL)

# Map it

library(leaflet)

leaflet() %>%
  addTiles() %>%
  addProviderTiles("Esri.WorldTopoMap", group = "Topo") %>%
  addProviderTiles("Esri.WorldImagery", group = "ESRI Aerial") %>%
  addCircleMarkers(data=df.SP, group="Hot Springs", radius = 4, opacity=1, fill = "darkblue",stroke=TRUE,
                   fillOpacity = 0.75, weight=2, fillColor = "yellow",
                   popup = paste0("Spring Name: ", df.SP$SpringName,
                                  "<br> Temp_F: ", df.SP$Temp_F,
                                  "<br> Area: ", df.SP$AREA)) %>%
  addLayersControl(
    baseGroups = c("Topo","ESRI Aerial"),
    overlayGroups = c("Hot SPrings"),
    options = layersControlOptions(collapsed = T))