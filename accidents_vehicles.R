###########################################################
# Atenció: comportament erràtic en el tema Encoding 
# si es treballa amb Windows. Afecta, fins i tot a la lectura
# del propi codi R
###########################################################
library(tidyverse)
library(dplyr) # Cargar la librería de manipulación de dataframes "dplyr"
library(stringr)
library(rgdal)
library(sf)
library(sqldf)

###########################################################
# 2010. Encoding erróneo YX en lloc d'XY
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripci¢ dia setmana	Dia setmana	Descripci¢ tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripci¢ causa vianant	Descripci¢ tipus de vehicle	Descripci¢ model	Descripci¢ marca	Descripci¢ color	Descripci¢ carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc10_veh_ini <- read.csv("./entrada/accidents_vehicles/2010_accidents_vehicles_gu_bcn_2010.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc10_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc10_veh <- select(acc10_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2011. Encoding erróneo YX en lloc d'XY
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripci� dia setmana	Dia setmana	Descripci� tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripci� causa vianant	Descripci� tipus de vehicle	Descripci� model	Descripci� marca	Descripci� color	Descripci� carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc11_veh_ini <- read.csv("./entrada/accidents_vehicles/2011_accidents_vehicles_gu_bcn_2011.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc11_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc11_veh <- select(acc11_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2012. Encoding erróneo YX en lloc d'XY
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripci� dia setmana	Dia setmana	Descripci� tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripci� causa vianant	Descripci� tipus de vehicle	Descripci� model	Descripci� marca	Descripci� color	Descripci� carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc12_veh_ini <- read.csv("./entrada/accidents_vehicles/2012_accidents_vehicles_gu_bcn_2012.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc12_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc12_veh <- select(acc12_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2013. Encoding erróneo YX en lloc d'XY
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripci� dia setmana	Dia setmana	Descripci� tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripci� causa vianant	Descripci� tipus de vehicle	Descripci� model	Descripci� marca	Descripci� color	Descripci� carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc13_veh_ini <- read.csv("./entrada/accidents_vehicles/2013_accidents_vehicles_gu_bcn_2013.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc13_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc13_veh <- select(acc13_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2014. Encoding erróneo YX en lloc d'XY
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripci� dia setmana	Dia setmana	Descripci� tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripci� causa vianant	Descripci� tipus de vehicle	Descripci� model	Descripci� marca	Descripci� color	Descripci� carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc14_veh_ini <- read.csv("./entrada/accidents_vehicles/2014_accidents_vehicles_gu_bcn_2014.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc14_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc14_veh <- select(acc14_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2015. Encoding ISO-8859-1
# Codi d'expedient	Codi districte	Nom districte	Codi barri	Nom barri	Codi carrer	Nom carrer	Num postal caption	Descripció dia setmana	Dia setmana	Descripció tipus dia	NK Any	Mes de any	Nom mes	Dia de mes	Hora de dia	Descripció causa vianant	Descripció tipus de vehicle	Descripció model	Descripció marca	Descripció color	Descripció carnet	Antiguitat carnet	Coordenada UTM (Y)	Coordenada UTM (X)
###########################################################

acc15_veh_ini <- read.csv("./entrada/accidents_vehicles/2015_accidents_vehicles_gu_bcn_2015.csv", 
                          fileEncoding = "ISO-8859-15", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc15_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_Y","UTM_X")

acc15_veh <- select(acc15_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2016. Encoding UTF-8
# Codi_expedient	Codi_districte	Nom_districte	Codi_barri	Nom_barri	Codi_carrer	Nom_carrer	Num_postal	Descripcio_dia_setmana	Dia_setmana	Descripcio_tipus_dia	Any	Mes_any	Nom_mes	Dia_mes	Hora_dia	Descripcio_causa_vianant	Descripcio_tipus_vehicle	Descripcio_model	Descripcio_marca	Descripcio_color	Descripcio_carnet	Antiguitat_carnet	Coordenada_UTM_X	Coordenada_UTM_Y	 Longitud	 Latitud
###########################################################

acc16_veh_ini <- read.csv("./entrada/accidents_vehicles/2016_accidents_vehicles_gu_bcn_.csv", 
                          fileEncoding = "UTF-8", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc16_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_X","UTM_Y","LON","LAT")

acc16_veh <- select(acc16_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,
                    DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET) 
###########################################################
# Elimino el registro con expediente nulo
###########################################################
acc16_veh <- filter(acc16_veh,NUM_EXPEDIENT != "")

###########################################################
# 2017. Encoding UTF-8
# Codi_expedient	Codi_districte	Nom_districte	Codi_barri	Nom_barri	Codi_carrer	Nom_carrer	Num_postal 	Descripcio_dia_setmana	Dia_setmana	Descripcio_tipus_dia	Any	Mes_any	Nom_mes	Dia_mes	Hora_dia	Descripcio_causa_vianant	Descripcio_tipus_vehicle	Descripcio_model	Descripcio_marca	Descripcio_color	Descripcio_carnet	Antiguitat_carnet	Coordenada_UTM_X	Coordenada_UTM_Y	Longitud	Latitud
###########################################################

acc17_veh_ini <- read.csv("./entrada/accidents_vehicles/2017_accidents_vehicles_gu_bcn_.csv", 
                          fileEncoding = "UTF-8", header=FALSE,skip=1,sep=",",dec=',',stringsAsFactors = FALSE)

names(acc17_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_X","UTM_Y","LON","LAT")

acc17_veh <- select(acc17_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

###########################################################
# 2018. Encoding UTF-8 Té un camp més (Desc_Torn)
# Codi_expedient	Codi_districte	Nom_districte	Codi_barri	Nom_barri	Codi_carrer	Nom_carrer	Num_postal 	Descripcio_dia_setmana	Dia_setmana	Descripcio_tipus_dia	Any	Mes_any	Nom_mes	Dia_mes	Hora_dia	Descripcio_torn	Descripcio_causa_vianant	Descripcio_tipus_vehicle	Descripcio_model	Descripcio_marca	Descripcio_color	Descripcio_carnet	Antiguitat_carnet	Coordenada_UTM_X	Coordenada_UTM_Y	Longitud	Latitud
###########################################################

acc18_veh_ini <- read.csv("./entrada/accidents_vehicles/2018_accidents_vehicles_gu_bcn_.csv", 
                          fileEncoding = "UTF-8", header=FALSE,skip=1,sep=",",dec='.',stringsAsFactors = FALSE)

names(acc18_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI","CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA","DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA","DESC_TORN",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR","DESC_CARNET","ANTIGUITAT_CARNET","UTM_X","UTM_Y","LON","LAT")

acc18_veh <- select(acc18_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

<<<<<<< HEAD
# 2019. Encoding UTF-8 Té un camp més (Desc_Torn)
# Codi_expedient	Codi_districte	Nom_districte	Codi_barri	Nom_barri	Codi_carrer	Nom_carrer	Num_postal 	Descripcio_dia_setmana	Dia_setmana	Descripcio_tipus_dia	Any	Mes_any	Nom_mes	Dia_mes	Hora_dia	Descripcio_torn	Descripcio_causa_vianant	Descripcio_tipus_vehicle	Descripcio_model	Descripcio_marca	Descripcio_color	Descripcio_carnet	Antiguitat_carnet	Coordenada_UTM_X	Coordenada_UTM_Y	Longitud	Latitud

acc19_veh_ini <- read.csv("./entrada/accidents_vehicles/2019_accidents_vehicles_gu_bcn_.csv", 
                          fileEncoding = "UTF-8", header=FALSE,skip=1,sep=",",dec='.',stringsAsFactors = FALSE)

names(acc19_veh_ini) = c("NUM_EXPEDIENT","CODI_DISTRICTE","NOM_DISTRICTE","CODI_BARRI","NOM_BARRI",
                         "CODI_VIA","NOM_VIA","NUM_VIA","DESC_DIA_SETMANA","DIA_SETMANA",
                         "DESC_TIPUS_DIA","NUM_ANY","MES_ANY","NOM_MES","DIA_MES","HORA_DIA","DESC_TORN",
                         "DESC_CAUSA_VIANANT","DESC_TIPUS_VEHICLE","DESC_MODEL","DESC_MARCA","DESC_COLOR",
                         "DESC_CARNET","ANTIGUITAT_CARNET","UTM_X","UTM_Y","LON","LAT")

acc19_veh <- select(acc19_veh_ini, NUM_EXPEDIENT,DESC_CAUSA_VIANANT,DESC_TIPUS_VEHICLE,DESC_MODEL,DESC_MARCA,DESC_COLOR,DESC_CARNET,ANTIGUITAT_CARNET)

=======
###########################################################
>>>>>>> ea244a3181504e92033f6827d3fe0979724040a0
# Junto todos los años para procesar globalmente
###########################################################

acc_veh <- rbind(acc10_veh,acc11_veh,acc12_veh,acc13_veh,acc14_veh,acc15_veh,acc16_veh,acc17_veh,acc18_veh,acc19_veh)

###########################################################
# Normalizo el tamaño del campo 
###########################################################

acc_veh$NUM_EXPEDIENT <- str_trim(acc_veh$NUM_EXPEDIENT, side = "both")


###########################################################
# sort(unique(acc_veh$DESC_CAUSA_VIANANT))
# sqldf("select DESC_CAUSA_VIANANT, count(*) from acc_veh group by DESC_CAUSA_VIANANT order by DESC_CAUSA_VIANANT")
###########################################################

acc_veh$DESC_CAUSA_VIANANT[str_detect(acc_veh$DESC_CAUSA_VIANANT,"causa del ")] <- "No és causa del vianant"
acc_veh$DESC_CAUSA_VIANANT[str_detect(acc_veh$DESC_CAUSA_VIANANT,"Transitar")] <- "Transitar a peu per la calçada"
acc_veh$DESC_CAUSA_VIANANT[str_detect(acc_veh$DESC_CAUSA_VIANANT,"el senyal del")] <- "Desobeir el senyal del semàfor"

acc_veh$DESC_CAUSA_VIANANT <- as.factor(acc_veh$DESC_CAUSA_VIANANT)

###########################################################
# sort(unique(acc_veh$DESC_TIPUS_VEHICLE))
# sqldf("select DESC_TIPUS_VEHICLE, count(*) from acc_veh group by DESC_TIPUS_VEHICLE order by DESC_TIPUS_VEHICLE")
###########################################################

acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Autob£s")] <- "Autobús"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Autob£s articulado")] <- "Autobús articulat"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Autobús articulado")] <- "Autobús articulat"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Cami¢n <= 3,5 Tm")] <- "Camió <= 3,5 Tm"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Cami¢n > 3,5 Tm")] <- "Camió > 3,5 Tm"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Camión <= 3,5 Tm")] <- "Camió <= 3,5 Tm"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Camión > 3,5 Tm")] <- "Camió > 3,5 Tm"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Desconegut")] <- NA
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Cuadriciclo <75cc")] <- "Quadricicle < 75 cc"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Cuadriciclo >=75cc")] <- "Quadricicle >= 75 cc"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Quadricicle > 75 cc")] <- "Quadricicle >= 75 cc"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Maquinaria agr")] <- "Maquinària agrícola"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Maquinaria de obras")] <- "Maquinària d'obres"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Microbus <= 17")] <- "Microbús <= 17"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Microbus <=17 plazas")] <- "Microbús <= 17"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Otros")] <- "Altres vehicles a motor"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Todo terreno")] <- "Tot terreny"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Tractocami")] <- "Tractor camió"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"tren")] <- "Tren o tramvia"
acc_veh$DESC_TIPUS_VEHICLE[str_detect(acc_veh$DESC_TIPUS_VEHICLE,"Turismo")] <- "Turisme"

###########################################################
# sort(unique(acc_veh$DESC_MODEL))
# Inabastable la possible correcció. Caldria revisar de nou els Encodings, si és que estan produïnt errors
# sqldf("select DESC_MARCA, DESC_MODEL, count(*) from acc_veh group by DESC_MARCA, DESC_MODEL order by 1,2")
#
# marca_modelo <- acc_veh %>% group_by(paste(DESC_MARCA,DESC_MODEL)) %>% summarise(count=n())
###########################################################

###########################################################
# sort(unique(acc_veh$DESC_MARCA))
# Inabastable la possible correcció. Caldria revisar de nou els Encodings, si és que estan produïnt errors
# sqldf("select DESC_MARCA, count(*) from acc_veh group by DESC_MARCA order by DESC_MARCA")
###########################################################

acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "S.Y.M","SYM", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "MECEDES", "MERCEDES", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "MERCEDES B", "MERCEDES", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "MERCEDES-BENZ", "MERCEDES", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "M A N", "MAN", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "M.A.N.", "MAN", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "K.T.M.", "KTM", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "JAGUAR LAN ROV", "JAGUAR", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "IVECO BUS", "IVECO", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "IVECO FIAT", "IVECO", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "H.R.D.", "HRD", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "B M W", "BMW", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "B.M.W.", "BMW", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "A.U.S.A.", "AUSA", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "A U D I", "AUDI", acc_veh$DESC_MARCA)
acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "A.U.D.I.", "AUDI", acc_veh$DESC_MARCA)

acc_veh$DESC_MARCA <- ifelse(acc_veh$DESC_MARCA == "ES DESCONEIX", "Desconegut", acc_veh$DESC_MARCA)

###########################################################
# sort(unique(acc_veh$DESC_COLOR))
# sqldf("select DESC_COLOR, count(*) from acc_veh group by DESC_COLOR order by DESC_COLOR")
###########################################################

acc_veh$DESC_COLOR[str_detect(acc_veh$DESC_COLOR,"granate")] <- "Granat"
acc_veh$DESC_COLOR[str_detect(acc_veh$DESC_COLOR,"Marr¢")] <- "Marró"

acc_veh$DESC_COLOR <- as.factor(acc_veh$DESC_COLOR)

###########################################################
# sort(unique(acc_veh$DESC_CARNET))
# sqldf("select DESC_CARNET, count(*) from acc_veh group by DESC_CARNET order by DESC_CARNET")
###########################################################

acc_veh$DESC_CARNET[str_detect(acc_veh$DESC_CARNET,"Es desconeix")] <- "Desconegut"
acc_veh$DESC_CARNET[str_detect(acc_veh$DESC_CARNET,"Llic")] <- "Llicència"

acc_veh$DESC_CARNET <- as.factor(acc_veh$DESC_CARNET)

###########################################################
# sort(unique(acc_veh$ANTIGUITAT_CARNET))
# sqldf("select ANTIGUITAT_CARNET, count(*) from acc_veh group by ANTIGUITAT_CARNET order by ANTIGUITAT_CARNET")
###########################################################

acc_veh$ANTIGUITAT_CARNET[str_detect(acc_veh$ANTIGUITAT_CARNET,"-")] <- NA
acc_veh$ANTIGUITAT_CARNET[str_detect(acc_veh$ANTIGUITAT_CARNET,"1018")] <- "10"
acc_veh$ANTIGUITAT_CARNET[str_detect(acc_veh$ANTIGUITAT_CARNET,"1818")] <- "18"

acc_veh$ANTIGUITAT_CARNET <- as.numeric(acc_veh$ANTIGUITAT_CARNET)

acc_veh %>% group_by(DESC_CAUSA_VIANANT) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(DESC_TIPUS_VEHICLE) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(DESC_MODEL) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(DESC_MARCA) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(DESC_COLOR) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(DESC_CARNET) %>% summarize(count=n()) ## Contempla los NA
acc_veh %>% group_by(ANTIGUITAT_CARNET) %>% summarize(count=n()) ## Contempla los NA
###########################################################
# Escribo el resultado en fichero de salida
###########################################################

write.table(
  acc_veh,
  file = "./salida/accidents_vehicles.csv",
  append = FALSE,
  sep = ",",
  dec = '.',
  na = "NA",
  row.names = FALSE,
  col.names = TRUE,
  fileEncoding = "UTF-8")

str(acc_veh)

###########################################################
# Elimino objetos que ya no necesarios para la confección de acc_tip
###########################################################
rm(list=c("acc10_veh","acc11_veh","acc12_veh","acc13_veh","acc14_veh",
          "acc15_veh","acc16_veh","acc17_veh","acc18_veh","acc19_veh",
          "acc10_veh_ini","acc11_veh_ini","acc12_veh_ini","acc13_veh_ini","acc14_veh_ini",
          "acc15_veh_ini","acc16_veh_ini","acc17_veh_ini","acc18_veh_ini","acc19_veh_ini"))
